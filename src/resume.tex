%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2019 Jan Küster
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%	
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and don't use a cv template
\documentclass[9pt, a4paper]{article}	


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

% we use utf8 since we want to build from any machine
\usepackage{fontspec}          % lualatex font engine

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xstring, xifthen}

%----------------------------------------------------------------------------------------
%	FONT BASICS
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
\defaultfontfeatures{Mapping=tex-text,Scale=MatchLowercase}
\setmainfont{Raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto} 

% set font default
% \renewcommand*\familydefault{\sfdefault} 	

% more font size definitions
\usepackage{moresize}

%----------------------------------------------------------------------------------------
%	FONT AWESOME ICONS
%---------------------------------------------------------------------------------------- 

% include the fontawesome icon set
\usepackage{fontawesome5}
\usepackage{emoji}
\usepackage[dvipsnames]{xcolor}
\usepackage{tikz}
\usepackage[]{svg} % <- also works
\svgpath{{../imgs/}} % <- using \svgpath to avoid warning
\usepackage{tabularx}

% use to vertically center content
% credits to: http://tex.stackexchange.com/questions/7219/how-to-vertically-center-two-images-next-to-each-other
\newcommand{\vcenteredinclude}[1]{\begingroup
\setbox0=\hbox{\includegraphics{#1}}%
\parbox{\wd0}{\box0}\endgroup}

% use to vertically center content
% credits to: http://tex.stackexchange.com/questions/7219/how-to-vertically-center-two-images-next-to-each-other
\newcommand*{\vcenteredhbox}[1]{\begingroup
\setbox0=\hbox{#1}\parbox{\wd0}{\box0}\endgroup}

% icon shortcut
\newcommand{\icon}[3] { 							
	\makebox(#2, #2){\textcolor{lightcol}{\faIcon{#1}}}
}	

% icon with text shortcut
\newcommand{\icontext}[4]{ 						
	\vcenteredhbox{\Large \icon{#1}{#2}{#3}}  ~ \vcenteredhbox{\textcolor{#4}{#3}}
}

% icon and text with website url
\newcommand{\iconhref}[4]{ 		
    \vcenteredhbox{\Large \href{#3}{\icon{#1}{#2}{#4}}}
}

% icon with email link
\newcommand{\iconemail}[5]{ 						
    \vcenteredhbox{\Large \icon{#1}{#2}{#5}}  ~ \href{mailto:#4}{\textcolor{#5}{#3}}
}

% icon with email link
\newcommand{\iconphone}[5]{ 						
    \vcenteredhbox{\Large \icon{#1}{#2}{#5}}  ~ \href{tel:#4}{\textcolor{#5}{#3}}
}

%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

% page outer frames (debug-only)
% \usepackage{showframe}		

% we use paracol to display breakable two columns
\usepackage{paracol}
%\usepackage{changepage}

% define page styles using geometry
\usepackage[a4paper, margin=0cm]{geometry}

\usepackage{fancyhdr}
% Set pagestyle to fancy
\pagestyle{fancy}

% Clear default fancyhdr settings
\fancyhf{}

% Set the footer to an empty rule
\renewcommand{\footrulewidth}{0pt}
\cfoot{}

% space between header and content
% \setlength{\headheight}{0pt}

% indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%---------------------------------------------------------------------------------------- 

% extended aligning of tabular cells
\usepackage{array}
\usepackage{tabularx}

% custom column right-align with fixed width
% use like p{size} but via x{size}
\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0.5cm}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}
\graphicspath{{../assets}}
% use this for floating figures
% \usepackage{wrapfig}
% \usepackage{float}
% \floatstyle{boxed} 
% \restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}

% banner definiton
\usepackage[most]{tcolorbox}
\newtcolorbox{banner}[3][]{  
    title={\textcolor{tipsColor}{\huge\faLightbulbO} #1\\},
    breakable,
    boxrule=0pt,
    nobeforeafter,
    blankest, 
    watermark graphics=#3, 
    watermark stretch=1,
    width=#2,
    height=4.4cm,
    left=0cm,
    right=0cm,
    top=0cm,
    bottom=0cm,
    boxsep=0cm
}
\newtcolorbox{col}[3][]{
    boxsep=0mm,
    boxrule=0pt,
    arc=0mm,
    height=25.2cm,
    top=0.3cm,
    nofloat,
    colback=#3,
    nobeforeafter,
    width=#2,
}
%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%---------------------------------------------------------------------------------------- 
\usepackage{transparent}
\usepackage{color}
% dark blue color
\definecolor{darkblue}{HTML}{051029}

% primary color
\definecolor{maincol}{HTML}{2b2f47}

% accent color, secondary
\definecolor{accentcol}{HTML}{0075bf}

% dark color
\definecolor{darkcol}{HTML}{0E0E0E}

% light color
\definecolor{lightcol}{RGB}{245,245,245}

% Package for links, must be the last package used
\usepackage[hidelinks]{hyperref}

% returns minipage width minus two times \fboxsep
% to keep padding included in width calculations
% can also be used for other boxes / environments
\newcommand{\mpwidth}{\linewidth-\fboxsep-\fboxsep}
	


%============================================================================%
%
%	CV COMMANDS
%
%============================================================================%

%----------------------------------------------------------------------------------------
%	 CV LIST
%----------------------------------------------------------------------------------------

% renders a standard latex list but abstracts away the environment definition (begin/end)
\newcommand{\cvlist}[1] {\vspace{-5pt}
	\begin{itemize}{
        \setlength{\itemsep}{-1.5pt}
        #1
    }\end{itemize}\vspace{-1em}
    
}

%----------------------------------------------------------------------------------------
%	 CV TEXT
%----------------------------------------------------------------------------------------

% base class to wrap any text based stuff here. Renders like a paragraph.
% Allows complex commands to be passed, too.
% param 1: *any
\newcommand{\cvtext}[1] {
	\begin{tabular*}{1\mpwidth}{p{0.98\mpwidth}}
		\parbox{1\mpwidth}{#1}
	\end{tabular*}
}

%----------------------------------------------------------------------------------------
%	CV SECTION
%----------------------------------------------------------------------------------------

% Renders a a CV section headline with a nice underline in main color.
% param 1: section title
\newcommand{\cvsection}[2] {
    \vspace{3pt}
	\cvtext{
		\textbf{\Large{\textcolor{maincol}{#2 \uppercase{#1}}}}\\[-4pt]
		\textcolor{maincol}{ \rule{2cm}{2pt} }
	}
    \vspace{8pt}
}

%----------------------------------------------------------------------------------------
%   SKILL
%----------------------------------------------------------------------------------------

% Renders a progress-bar to indicate a certain skill in percent.
% param 1: name of the skill / tech / etc.
% param 2: level (for example in years)
% param 3: percent, values range from 0 to 1
\newcommand{\cvskill}[3] {
    \vspace{2pt}
	\begin{tabular*}{1\mpwidth}{m{0.15\mpwidth}  m{0.85\mpwidth}}
 		\textcolor{darkcol}{\textbf{#1}} & \textcolor{maincol}{#2}
	\end{tabular*}\\
}

%----------------------------------------------------------------------------------------
%	META SKILL
%----------------------------------------------------------------------------------------

% Renders a progress-bar to indicate a certain skill in percent.
% param 1: name of the skill / tech / etc.
% param 2: level (for example in years)
% param 3: percent, values range from 0 to 1
\newcommand{\cvmetaskill}[3] {
    \vspace{2pt}
    \begin{tabular*}{1\mpwidth}{p{\mpwidth}}
        \textcolor{darkcol}{\textbf{#1}} \\
        \textcolor{maincol}{#2}
   \end{tabular*}\\
}

%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% Renders a table and a paragraph (cvtext) wrapped in a parbox (to ensure minimum content
% is glued together when a pagebreak appears).
% Additional Information can be passed in text or list form (or other environments).
% the work you did
% param 1: time-frame i.e. Sep 14 - Jan 15 etc.
% param 2:	 event name (job position etc.)
% param 3: Customer, Employer, Industry
% param 4: Short description
% param 5: work done (optional)
% param 6: technologies include (optional)
% param 7: achievements (optional)
\newcommand{\cvevent}[7] {
	
	% we wrap this part in a parbox, so title and description are not separated on a pagebreak
	% if you need more control on page breaks, remove the parbox
	\begin{tabularx}{\textwidth}{r X l}
		\large{\textcolor{darkcol}{\textbf{#2}}} & {\textcolor{maincol}{\textbf{#3}}} & \large{\colorbox{maincol}{{~\textcolor{white}{#1}~}}}\\
	\end{tabularx}\\[-0.5em]

	\ifthenelse{\isempty{#4}}{}{
		\cvtext{#4}	
	}

	\ifthenelse{\isempty{#5}}{}{
		{#5}
	}

	\ifthenelse{\isempty{#6}}{}{
		\cvtext{\textbf{Technos used:}}\\
		{#6}
	}

	\ifthenelse{\isempty{#7}}{}{
		\cvtext{\textbf{Reached achievements:}}\\
		{#7}
	}
    \vspace{5pt}
}

%----------------------------------------------------------------------------------------
%	 CV META EVENT
%----------------------------------------------------------------------------------------

% Renders a CV event on the sidebar
% param 1: title
% param 2: subtitle (optional)
% param 3: customer, employer, etc,. (optional)
% param 4: info text (optional)
\newcommand{\cvmetaevent}[4] {
	\textcolor{maincol} {\cvtext{\textbf{#1}}}

	\ifthenelse{\isempty{#2}}{}{
	\textcolor{darkcol} {\cvtext{\textbf{#2}} }
	}

	\ifthenelse{\isempty{#3}}{}{
        \vspace{1mm}
		\cvtext{{ \textcolor{darkcol} {#3} }}
	}
	\cvtext{#4}\\
}

%---------------------------------------------------------------------------------------
%	QR CODE
%----------------------------------------------------------------------------------------

% Renders a qrcode image (centered, relative to the parentwidth)
% param 1: percent width, from 0 to 1
\newcommand{\cvqrcode}[1] {
	\begin{center}
		\includegraphics[width={#1}\mpwidth]{qrcode}
	\end{center}
}


%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}
\setemojifont{TwemojiMozilla}
%---------------------------------------------------------------------------------------
%	TITLE  HEADER
%----------------------------------------------------------------------------------------

\begin{banner}{\paperwidth}{banner.jpg}
    \vspace{5mm}
    \hspace{0.5cm}
    \begin{minipage}{3cm}
        \begin{tikzpicture}
            \clip (0,0) circle (1.5cm) node
                {\includegraphics[width=3cm]{profile.jpg}};
        \end{tikzpicture}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}{10.5cm}
        \color{lightcol}
        \fontsize{32pt}{40pt}{\textbf{\uppercase{Mathieu Dupoux}}}\\[4mm]
        \large{Futur ingénieur en informatique spécialisé en cybersécurité et réseaux, je recherche un stage de fin d'étude entre février et août 2024}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}{5.5cm}
        %---------------------------------------------------------------------------------------
        %	Contact
        %----------------------------------------------------------------------------------------

        \icontext{map-marker-alt}{12}{Clermont-Ferrand/Lyon}{lightcol}\\[3mm]
        \iconemail{envelope}{12}{contact@mathieudupoux.fr}{contact@mathieudupoux.fr}{lightcol}\\[3mm]
        \iconphone{mobile-alt}{12}{+33 614 752 811}{+33614752811}{lightcol}\\[3mm]
        \iconhref{linkedin}{12}{https://www.linkedin.com/in/mathieudupoux}{lightcol}
        \iconhref{github}{12}{https://github.com/mathieudupoux}{lightcol}
        \iconhref{globe}{12}{https://www.mathieudupoux.fr}{lightcol}
        \textcolor{lightcol}{mathieudupoux}\\[3mm]
        \icontext{address-card}{12}{Permis B}{lightcol}

        %\cvqrcode{0.7}
    \end{minipage}
\end{banner}
\begin{col}{0.3\paperwidth}{lightcol}
    %---------------------------------------------------------------------------------------
    %	META SKILLS
    %----------------------------------------------------------------------------------------
    \cvsection{Compétences}{\faIcon{code}}
    \cvmetaskill{Administation système}{Unix/Linux, SSH, systemd, Apache/Nginx, nftables, Docker, QEMU/KVM}\\
    \cvmetaskill{Réseau}{TCP/IP, modèle OSI, Socket API (C), API REST, Analyse réseau (nmap, Wireshark/tcpdump)}\\
    \cvmetaskill{Langages}{C/C++, JS/TS, Python, Java}\\
    \cvmetaskill{Base de donnée}{MySQL, SQLite, ORM Sequelize}\\
    \cvmetaskill{Web + Frameworks}{HTML5, CSS3, VueJS, PrimeVue, ExpressJS, Flask}\\
    \cvmetaskill{Déploiement}{GitLab (avec CI/CD), Terraform, Ansible}\\
    \cvmetaskill{Bureautique}{LibreOffice, MSOffice, GSuite, \LaTeX}\\

    \vspace{5pt}
    \cvsection{Certifications}{\faIcon{code}}

    \begin{tabular*}{1\mpwidth}{m{0.65\mpwidth}  m{0.35\mpwidth}}
        \textbf{SecNumAcadémie} & juil. 2023 \\[1mm]
        \textbf{IELTS (7.5/9)} & janv. 2023 \\[1mm]
        \textbf{PSC1} & avril. 2019
    \end{tabular*}\\



    \vspace{6pt}
    \cvsection{Langues}{\faIcon{language}}

    \begin{tabular*}{1\mpwidth}{m{0.15\mpwidth}  m{0.85\mpwidth}}
        \textcolor{darkcol}{\Large{\emoji{flag-france}}} & \textcolor{maincol}{Langue maternelle} \\[2mm]
        \textcolor{darkcol}{\Large{\emoji{flag-united-kingdom}}} & \textcolor{maincol}{C1 (IELTS 7.5/9)} \\[2mm]
        \textcolor{darkcol}{\Large{\emoji{flag-germany}}} & \textcolor{maincol}{Débutant (A2)}
    \end{tabular*}\\

    %---------------------------------------------------------------------------------------
    %	Centres d'intérêts
    %----------------------------------------------------------------------------------------

    \vspace{6pt}
    \cvsection{Activités}{\faIcon{heart}}
    \cvlist{
        \item[\faIcon{bicycle}] \textbf{Cyclisme} \\ Animateur cycliste au Vélo Club Riomois et à Cycl'Eirb
        \item[\faIcon{book}] \textbf{Partage de savoirs} \\ Responsable formations Eirbware (informatique pour primo-entrants)
        \item[\faIcon{glass-cheers}] \textbf{Gala Mos'Fête}\\ Coordinateur du Gala Most'Fête 2023
        \item[\faIcon{compass}] \textbf{Cartographie} \\ Contributeur OSM

    }
\end{col}
\begin{col}{0.726\paperwidth}{white}

    %---------------------------------------------------------------------------------------
    %	PROFILE
    %---------------------------------------------------------------------------------------

    %---------------------------------------------------------------------------------------
    %	WORK EXPERIENCE
    %----------------------------------------------------------------------------------------
    \cvsection{Expériences professionnelles}{\faIcon{briefcase}}
    \cvevent
    {juin - août 2023}
    {Développeur frontend}
    {Universität Rostock - VAC ~ \emoji{flag-germany}}
    {\vspace{1mm}Programmation d'une interface web pour la visualisation des données de jumeaux numériques
        d'environnements sous-marins dans le cadre du projet OTC-Digital Twin \& Analytics (OTC-DaTA) }
    {\cvlist{
            \item Conception de visualisation de données probabilistes avec Three.JS
            \item Intégration au sein d'une interface utilisateur interactive avec VueJS
            \item Mise en place d'un prototype de CI/CD et d'une documentation automatisée
        }}
    {}{}

    \cvevent
    {fév. 2022 - avril 2023}
    {Chargé SI}
    {Aquitaine Électronique Informatique}
    {Maintenance et développement du système d'information de la Junior-Entreprise}
    {\cvlist{
            \item Administation système Linux : migration de serveur, services Docker
            \item Nouvelle stratégie de sauvegarde avec BorgBackup
            \item Sensibilisation des membres aux enjeux de cyber-sécurité
        }}{}{}

    \cvevent
    {Étés 2018-2022}
    {Monteur-Tuyauteur}
    {EFG Guillot}
    {Montage des systèmes de protection incendie (sprinklers, tuyauterie, essais...)}{}
    {}{}

    % \cvevent
    % {Sep 15 - NOW}
    % {DevOps/FullStack developer}
    % {Research and Development}
    % {A large IAM project required an intuitive interface for role-based access and rights management. At the same time, new workflows for role based access, life time and monitoring had to be established}
    % {\cvlist{
    %         \item Creation of a web portal for role- and rights management
    %         \item Establishing a connection to the existing MicroFocus role solution
    %         \item Development of new role-based workfows and processes, as well as training and support
    %         \item Maintenance of existing infrastructure
    %     }}
    % {\cvlist {
    %         \item Django for the roles- and rights management tool backend (backend is a REST interface)
    %         \item Angular for the easy frontend interaction
    %         \item HTML5/CSS3/Bootstrap3 for the frontend
    %         \item Ansible + Docker for 1-click deployments
    %     }}
    % {\cvlist{
    %         \item A web based tool for an intuitive role assignment and administration
    %         \item Online overview of company structures, projects, etc
    %         \item Tools for role review, reporting and troubleshooting
    %     }}

    %---------------------------------------------------------------------------------------
    %	CERTIFICATION
    %----------------------------------------------------------------------------------------

    %\cvsection{Certifications}
    %---------------------------------------------------------------------------------------
    %	ORGANIZATION
    %----------------------------------------------------------------------------------------
    \cvsection{Projets Académiques}{\faIcon{university}}
    \cvevent
    {oct. 2023 - jan 2024}
    {Hotspot Wifi EvilTwin}
    {Semestre 9}
    {Création d'un démonstrateur de hotspot Wifi piégé usurpateur de WiFi public :
    }{
        \cvlist{
            \item Copie du portail captif avec formulaire de connexion piégé
            \item Analyse de trafic TCP (requête DNS, HTTP), récupération d'identifiants
            \item Lancement d'attaque automatique
            \item Visualisation "grand public" des attaque en temps réel (Flask)
        }
    }{}{}

    \cvevent
    {oct. 2023 - jan 2024}
    {Plateforme CTF}
    {Semestre 9}
    {Développement de la plateforme CTF RSR vers sa version 3.0}{
        \cvlist{
            \item Ajout du support de challenge en VM (Terraform)
            \item Création d'un CTF avec élévation de privilège par LinPEAS
            \item Déploiement automatisée (Gitlab CI/CD, Terraform, Docker)
        }
    }{}{}

    \cvevent
    {nov. 2022 - mai 2023}
    {Nouvel ERP pour AEI}
    {Semestre 8}
    {Conception et développement d'un nouvel ERP (TS, ExpressJS, Vue, Sequelize) pour la gestion
        d'activité d'AEI, la Junior-Entreprise de l'ENSEIRB-Matméca}{}{}{}

    \cvevent
    {mars - mai 2023}
    {Gestion centralisée d'un aquarium}
    {Semestre 8}
    {Création d'un aquarium à différentes vues en C++ asservies par
        par un serveur central en réseau par socket API TCP programmé en C }{}
    {}{}

    % \cvevent
    % {mars - mai 2023}
    % {Threads en espace utilisateurs}
    % {Semestre 8}
    % {Réimplémentation de zéro d'une bibliothèque de threads utilisateurs avec mutex et préemption}{}
    % {}{}\vspace{-0.5em}

    % \cvevent
    % {feb. - avril 2023}
    % {IA symbolique et ML pour le jeu de Go}
    % {Semestre 8}
    % {Conception d'une AI en Python basé sur l'algorithme alpha-bêta et
    %     un réseau de neurones implémenté avec Keras pour l'évaluation des plateaux}{}
    % {}{}

    % \cvevent
    % {oct. - déc. 2022}
    % {Communauté de joueurs}
    % {Semestre 7}
    % {Développement d'une base de donnée et d'une interface pour gérer une communauté de rôlistes}{}
    % {}{}

    % \cvevent
    % {mars - mai 2022}
    % {Flood-filling game}
    % {Semestre 6}
    % {Implémentation des règles et mécaniques du jeu Flood-filling game
    %     avec structure et algorithmique des graphes en C}{}
    % {}{}\vspace{-0.2em}

    % \cvevent
    % {oct. 2021 - jan. 2022}
    % {Automate cellulaire}
    % {Semestre 5}
    % {\vspace{-0.5em}Implémentation d'un automate cellulaire en C avec règles modulaires}{}
    % {}{}

    % \cvevent
    % {Mar. - mai. 2022}
    % {Robot programming}
    % {Semestre 6}
    % {Developed an interface to program a robot with a simplified
    %     functional programming language to solve a labyrinth-like game}{}
    % {}{}

    %---------------------------------------------------------------------------------------
    %	Education
    %----------------------------------------------------------------------------------------
    \cvsection{Formation}{\faIcon{user-graduate}}

    \cvmetaevent
    {2021 - (2024)}
    {Ingénieur en Informatique spécialité cybeR-sécurité, Systèmes et Réseaux (RSR) }
    {ENSEIRB-Matmeca, Bordeaux}
    {}
    \vspace{-1.5em}

    \begin{minipage}{0.45\mpwidth}
        \cvmetaevent
        {2019 - 2021}
        {CPGE MPSI-MP}
        {Lycée Lafayette, Clermont-Ferrand}
        {}
    \end{minipage}
    \hspace{0.01\mpwidth}
    \begin{minipage}{0.54\mpwidth}
        \cvmetaevent
        {2019}
        {Baccalauréat S Sciences de l'Ingénieur}
        {Lycée Pierre Joël Bonté, Riom (63)}
        {Mention Très Bien avec félicitations du jury}
    \end{minipage}

\end{col}
\end{document}

